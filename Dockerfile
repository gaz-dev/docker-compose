# Compos eis now part of docker base image
ARG docker_ce_version
FROM docker:${docker_ce_version}

ENTRYPOINT []

RUN old_repo="$(cat /etc/apk/repositories)" \
 && sed -i s#//.*.alpinelinux.org#//uk.alpinelinux.org#g /etc/apk/repositories \
 && echo "$old_repo" >> /etc/apk/repositories \
 && apk add --no-cache bash openssh-client ca-certificates make curl wget gettext jq \
 && ln -sf /bin/bash /bin/sh \
 && echo -e "Host *\n   StrictHostKeyChecking no\n" >>/etc/ssh/ssh_config \
 && mkdir -p -m 700 ~/.ssh \
 && docker -v \
 && docker-compose -v

CMD ["bash"]
