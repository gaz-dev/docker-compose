# Docker image with docker compose

This image was originaly built for docker and compose with docker over ssh, However docker base image now contains both binaries.
As this image is used by many repositories now we are keeping the name same for compatiblity.

## Pull
docker pull genomicsengland/docker-compose:latest

## Description
docker version (latest based on github release tag)  
docker-compose version (latest based on available in pypi)  
Image Based on alpine, Removed unused packages to reduce size  
Include bash & openssh-client  

## SSH support
suppressed paramiko warning about deprecation  
Please run a dummy command over ssh to add it in known_hosts before docker-compose over ssh is run.
